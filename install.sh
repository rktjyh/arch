#!/bin/bash
# This script installs the base arch system.

# Check if the system is booted in UEFI mode.
# if [ ! -d "/sys/firmware/efi/efivars" ]
# then
#    # If not, then exit.
#    echo "[Error!] Reboot in UEFI mode and try again."
#    exit 1
#fi

# Delete existing partition table.
wipefs -a -f /dev/sda

# Partition disks :
# 1. /dev/sda1 for EFI  partition taking +512M.
# 2. /dev/sda2 for Root partition taking rest of the disk.

(
echo n      # Create new partition (for EFI)
echo p      # Set partition type to primary
echo        # Set default partition number.
echo        # Set default first sector.
echo +512   # Set +512 as last sector.
echo n      # Create new partition (for Root)
echo p      # Set partition type to primary.
echo        # Set default partition number.
echo        # Set default first sector.
echo        # Set default last sector (use rest of the disk).
echo w      # Write changes.
) | fdisk /dev/sda -w always -W always

# Format the created paritions :

mkfs.fat -F32 /dev/sda1 # EFI  Partition
mkfs.ext4     /dev/sda2 # Root Partition

# Mount the filesystem
mount /dev/sda2 /mnt

# Update mirrorlist ? Not needed.
# On the live system, after connecting to the internet,
# reflector automatically updates the mirror list.

# Install essential packages
pacstrap /mnt base base-devel linux linux-firmware nano

# Generate fstab file.
genfstab -U /mnt >> /mnt/etc/fstab

# Download setup script from repo into /mnt.
curl https://gitlab.com/sanganak_shilpkar/arch/-/raw/main/setup.sh -o /mnt/setup.sh

# Run the setup script from /mnt with arch-chroot.
arch-chroot /mnt bash setup.sh

# Unmount paritions & reboot.
umount -R /mnt
reboot
