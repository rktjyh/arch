#!/bin/bash
# This script sets up neovim for everyday use.

# Remove old config directory.
rm -rf ~/.config/nvim

# Install prerequisites.
sudo pacman -Sy nodejs neovim --noconfirm --needed;

# Create directory for config files.
mkdir ~/.config/nvim

# Create directory for undo files within nvim config directory.
mkdir ~/.config/nvim/undodir

# Create directory for plugins within nvim config directory.
mkdir ~/.config/nvim/plugged

# Fetch "init.vim" config file.
curl https://gitlab.com/sanganak_shilpkar/arch/-/raw/main/.config/nvim/init.vim -o ~/.config/nvim/init.vim

# Download plug.vim into the "autoload" directory
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

# Source config file
nvim -p -c 'PlugInstall | source ~/.config/nvim/init.vim | quitall'
nvim -p -c 'CocUpdate'

