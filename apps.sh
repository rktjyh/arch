#!/bin/bash
# This script installs a custom set of apps.

# Sync databases
pacman -Syu

# Install Arch Apps :

apps=(

    # essentials
    'thunar'                # file manager
    'mousepad'              # text editor
    'okular'                # doc viewer
    'ristretto'             # image viewer
    'firefox'               # primary browser
    'chromium'              # secondary browser
    'torbrowser-launcher'   # tor launcher
    'shutter'               # screenshot tool
    'vlc'                   # media player
    'foliate'               # ebook reader
    'fragments'             # torrent client
    'gnome-calculator'      # calculator
    'gnome-multi-writer'    # iso file writer
    'gnome-sound-recorder'  # sound recorder
    'gnome-podcasts'        # podcasts app
    'curtail'               # image compressor
    'gcolor3'               # color picker
    'drawing'               # image editor
    'mypaint'               # raster painting
    'seahorse'              # encryption keys

    # cli
    'htop'                  # process viewer
    'dust'                  # disk usage
    'fd'                    # find alternative


    # extras
    # 'simplescreenrecorder'  # screen recorder
    # 'peek'                  # GIF recorder
    # 'obs-studio'            # screen cast/record
    # 'audacity'              # audio editor
    # 'kdenlive'              # video editor
)

for app in "${apps[@]}"; do
    pacman -S "$app"
done

# Install Flatpaks :

apps=(
    'com.github.geigi.cozy'         # audio book player
    'org.onlyoffice.desktopeditors' # only office suite
    'org.gaphor.Gaphor'             # diagram tool
    'com.github.phase1geo.minder'   # mind map
    'com.belmoussaoui.Decoder'      # QR decoder
)

for app in "${apps[@]}"; do
    flatpak install -y --noninteractive flathub "$app"
done

