# Arch
 
This project consists of scripts to install a minimal arch system that just works. The configurations are hardcoded as per my preference and no options are provided to choose from, since this is an automation script, not an arch linux installer.

Need a work environment for super human productivity ? Fork it, tweak it, and make it your own !

Screenshot 01  |  Screenshot 02
:-------------------------:|:-------------------------:
![](assets/unixporn-02-min.png)  |  ![](assets/unixporn-01-min.png)

## Pre-installation

* Download the arch iso from [this](https://archlinux.org/download/) page and [verify](https://wiki.archlinux.org/title/Installation_guide#Verify_signature) the signature of your download.
* Prepare the [USB flash installation medium](https://wiki.archlinux.org/title/USB_flash_installation_medium). (Do not use [these](https://wiki.archlinux.org/title/USB_flash_installation_medium#Inadvisable_methods) methods.)
* Boot the live environment in UEFI mode. Disable Secure boot as Arch linux images do not support secure boot.
* Connect to the internet using one of following techniques :
    * Plug in an ethernet cable.
    * Authenticate to a wireless network using [iwctl](https://wiki.archlinux.org/title/Iwd#iwctl).
    * Connect to a mobile network with the [mmcli](https://wiki.archlinux.org/title/Mobile_broadband_modem#ModemManager) utility.

To check if you are connected to the internet. Run the command `ping archlinux.org`.

## Installation

Use the below command from a live environment shell to [install](https://gitlab.com/sanganak_shilpkar/arch/-/blob/main/install.sh) and [setup](https://gitlab.com/sanganak_shilpkar/arch/-/blob/main/setup.sh) vanilla arch for everyday use.

```
curl gitlab.com/sanganak_shilpkar/arch/-/raw/main/install.sh -o install.sh; sh install.sh
```

* You will be prompted a few times to specify the hostname, username, passwd etc.
* Once the installation is complete. The machine will automatically reboot into arch and you will see the login page.

## Post-installation

<details><summary>Change Appearance</summary>
<br>

* Using `sudo lxappearance` to tweak theme, icon, font, cursor etc.
* Using `sudo lightdm-gtk-greeter-settings` to tweak login greeter.

</details>

<details><summary>Setup Shell Style</summary>
<br>

The script installs [fish](https://fishshell.com/) shell along with the [tide](https://github.com/IlanCosman/tide) prompt which you can configure using below command :

```bash
tide configure
```

</details>

<details><summary>Setup Neovim Editor</summary>
<br>

Use [Neovim](https://neovim.io/) ? Try the below command to auto-install my setup that just works. 

```bash
curl https://gitlab.com/sanganak_shilpkar/arch/-/raw/main/editor.sh -o editor.sh; sh editor.sh
```

</details>

<details><summary>Setup Git & SSH </summary>
<br>

Yes, yes and yes. You can do it manually but if you're lazy like me, use the below command to setup git & SSH :

```bash
curl https://gitlab.com/sanganak_shilpkar/arch/-/raw/main/git.sh -o git.sh; sh git.sh
```

</details>

<details><summary> Install Apps </summary>
<br>

These are all the apps that I use. Take a look, you might find something useful.

Essentials :
* File Manager : thunar.
* Text Editor : mousepad
* Document Reader : okular
* Image Viewer : ristretto
* Media Player : vlc
* Browser : firefox, chromium, torbrowser-launcher
* Calculator : gnome-calculator

Content Creation :
* Screen Capture : shutter, simplescreenrecorder
* GIF Recorder : peek
* Screencast : obs-studio
* Sound Recorder : gnome-sound-recorder
* Audio Editor : audacity
* Video Editor : kdenlive

Digital Art
* Color Picker : gcolor3
* Basic Image Editor : drawing
* Digital Painter : mypaint

Productivity
* Book Reader : foliate
* Audio Book Player : com.github.geigi.cozy `flatpak`
* Podcast Player : gnome-podcasts
* Office : org.onlyoffice.desktopeditors `flatpak`

Design :
* Diagrams : org.gaphor.Gaphor `flatpak`
* Mind Maps : com.github.phase1geo.minder `flatpak`
* Stylus Note Taking : xournalpp

Tools :
* Taskmanager : Htop
* Torrent Client : fragments
* Image Compressor : curtail
* ISO Flasher : gnome-multi-writer
* Display & Control Android : scrcpy `paru`

Use the below command to install all the above mentioned apps.

```bash
curl https://gitlab.com/sanganak_shilpkar/arch/-/raw/main/apps.sh -o apps.sh; sh apps.sh
```

</details>

## Project status

This is a long-term passive project. I contribute to this project if and when I come across something useful.
